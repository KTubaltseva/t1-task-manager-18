package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.AbstractAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email) throws AbstractAlreadyExistsException, AbstractFieldException;

    void login(String login, String password) throws AbstractException;

    void logout();

    boolean isAuth();

    String getUserId() throws AuthRequiredException;

    User getUser() throws UserNotFoundException, IdEmptyException, AuthRequiredException;

}
