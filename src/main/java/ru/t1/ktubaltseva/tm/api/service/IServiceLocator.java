package ru.t1.ktubaltseva.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IUserService getUserService();

    IAuthService getAuthService();

}
