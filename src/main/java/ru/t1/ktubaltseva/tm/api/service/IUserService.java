package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.AbstractAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.LoginAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.List;

public interface IUserService {

    User create(String login, String password) throws AbstractFieldException, AbstractAlreadyExistsException;

    User create(
            String login,
            String password,
            String email
    ) throws AbstractFieldException, AbstractAlreadyExistsException;

    User create(
            String login,
            String password,
            Role role
    ) throws AbstractFieldException, LoginAlreadyExistsException;

    User add(User user) throws UserNotFoundException;

    List<User> findAll();

    User findById(String id) throws IdEmptyException, UserNotFoundException;

    User findByLogin(String login) throws LoginEmptyException, UserNotFoundException;

    User findByEmail(String email) throws EmailEmptyException, UserNotFoundException;

    User remove(User user) throws UserNotFoundException;

    User removeByLogin(String login) throws UserNotFoundException, LoginEmptyException;

    User removeByEmail(String email) throws EmailEmptyException, UserNotFoundException;

    User setPassword(String id, String password) throws UserNotFoundException, AbstractFieldException;

    User updateUser(
            String id,
            String firstName,
            String middleName,
            String lastName
    ) throws UserNotFoundException, IdEmptyException;

    Boolean isLoginExists(String login) throws LoginEmptyException;

    Boolean isEmailExists(String email) throws EmailEmptyException;

}
