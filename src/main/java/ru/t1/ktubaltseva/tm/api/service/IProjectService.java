package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    Project add(Project project) throws AbstractException;

    void clear();

    Project create(String name, String description) throws AbstractException;

    Project create(String name) throws AbstractException;

    Project changeProjectStatusById(String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws AbstractException;

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAll(Sort sort);

    Project findOneById(String id) throws AbstractException;

    Project findOneByIndex(Integer index) throws AbstractException;

    void remove(Project project) throws AbstractException;

    Project removeById(String id) throws AbstractException;

    Project removeByIndex(Integer Index) throws AbstractException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

}
