package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.service.IAuthService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.auth.LoginIncorrectException;
import ru.t1.ktubaltseva.tm.exception.auth.PasswordIncorrectException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.AbstractAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.PasswordEmptyException;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(String login, String password, String email) throws AbstractAlreadyExistsException, AbstractFieldException {
        return userService.create(login, password, email);
    }

    @Override
    public void login(String login, String password) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        User user;
        try {
            user = userService.findByLogin(login);
        } catch (UserNotFoundException e) {
            throw new LoginIncorrectException();
        }
        final String passwordHash = HashUtil.salt(password);
        if (!passwordHash.equals(user.getPasswordHash())) throw new PasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() throws AuthRequiredException {
        if (!isAuth()) throw new AuthRequiredException();
        return userId;
    }

    @Override
    public User getUser() throws UserNotFoundException, AuthRequiredException {
        try {
            return userService.findById(userId);
        } catch (IdEmptyException e) {
            throw new AuthRequiredException();
        }
    }

}
