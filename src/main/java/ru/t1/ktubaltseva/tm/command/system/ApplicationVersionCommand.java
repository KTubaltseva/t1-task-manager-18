package ru.t1.ktubaltseva.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private final String NAME = "version";

    private final String ARGUMENT = "-v";

    private final String DESC = "Display program version.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.18.0");
    }

}
