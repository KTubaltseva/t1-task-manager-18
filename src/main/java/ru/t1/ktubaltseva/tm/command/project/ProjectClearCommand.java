package ru.t1.ktubaltseva.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private final String NAME = "project-clear";

    private final String DESC = "Clear project list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAN]");
        getProjectService().clear();
    }

}
