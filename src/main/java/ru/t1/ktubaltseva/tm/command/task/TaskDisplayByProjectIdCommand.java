package ru.t1.ktubaltseva.tm.command.task;

import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.List;

public final class TaskDisplayByProjectIdCommand extends AbstractTaskCommand {

    private final String NAME = "task-display-by-project-id";

    private final String DESC = "Display task list by project id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY TASKS BY PROJECT ID]");
        System.out.println("[ENTER ID]:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }

}
