package ru.t1.ktubaltseva.tm.command.project;

import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    private final String NAME = "project-start-by-index";

    private final String DESC = "Start project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextInt() - 1;
        getProjectService().changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

}
