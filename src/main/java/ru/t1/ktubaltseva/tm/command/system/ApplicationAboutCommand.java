package ru.t1.ktubaltseva.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private final String NAME = "about";

    private final String ARGUMENT = "-a";

    private final String DESC = "Display developer info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Tubaltseva Ksenia");
        System.out.println("email: ktubaltseva@t1-consulting.ru");
    }

}
