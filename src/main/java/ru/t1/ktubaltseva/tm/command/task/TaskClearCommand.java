package ru.t1.ktubaltseva.tm.command.task;

import ru.t1.ktubaltseva.tm.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    private final String NAME = "task-clear";

    private final String DESC = "Clear task list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
    }

}
