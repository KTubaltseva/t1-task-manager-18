package ru.t1.ktubaltseva.tm.command.user;

import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    private final String NAME = "user-registry";

    private final String DESC = "Registry user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REGISTRY USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        displayUser(user);
    }

}
