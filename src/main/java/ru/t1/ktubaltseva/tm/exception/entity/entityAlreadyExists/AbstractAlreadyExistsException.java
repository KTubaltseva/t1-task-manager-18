package ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists;

import ru.t1.ktubaltseva.tm.exception.AbstractException;

public abstract class AbstractAlreadyExistsException extends AbstractException {

    public AbstractAlreadyExistsException() {
    }

    public AbstractAlreadyExistsException(String message) {
        super(message);
    }

    public AbstractAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public AbstractAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
