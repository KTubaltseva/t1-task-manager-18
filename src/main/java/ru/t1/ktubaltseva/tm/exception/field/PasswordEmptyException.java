package ru.t1.ktubaltseva.tm.exception.field;

public class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}